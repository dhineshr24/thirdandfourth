package util;

import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;

public interface SequenceFileWriterInterface {
    void setProtoFields(CSVRecord record);
    void writeToSequenceFile();
    SequenceFile.Writer getSequenceWriter(Configuration config, Path path) throws IOException;
}
