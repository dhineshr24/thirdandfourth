package com.protoassignments.second;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class BulkLoadBuildingMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

    private static final byte[] CF_NAME = Bytes.toBytes("information");

    private final byte[][] QUAL_BYTES = {"building_code".getBytes(), "total_floors".getBytes(),"companies_in_the_building".getBytes(),
            "cafeteria_code".getBytes()};

    @Override
    protected void map(LongWritable key, Text value,
                       Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context) throws IOException, InterruptedException {

        if (value.getLength() == 0) {
            return;
        }

        //building_code, total_floors, companies_in_the_building, cafteria_code

        byte[] rowKey = Bytes.toBytes(String.valueOf(key));
        String data[] = value.toString().split(",");
        Put put = new Put(rowKey);
        put.addColumn(CF_NAME, QUAL_BYTES[0], Bytes.toBytes(data[0]));
        put.addColumn(CF_NAME, QUAL_BYTES[1], Bytes.toBytes(data[1]));
        put.addColumn(CF_NAME, QUAL_BYTES[2], Bytes.toBytes(data[2]));
        put.addColumn(CF_NAME, QUAL_BYTES[3], Bytes.toBytes(data[3]));

        context.write(new ImmutableBytesWritable(rowKey), put);

    }
}
