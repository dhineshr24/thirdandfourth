package com.protoassignments.first;

import com.protoobjects.Employee;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import util.SequenceFileWriterInterface;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class EmployeeProtoData implements SequenceFileWriterInterface {

    private Path OUTPUT;
    private String employeeFile;
    private ArrayList<Employee.Builder> employees;

    FileSystem fileSystem;
    SequenceFile.Writer writer = null;
    Configuration conf;

    public EmployeeProtoData(String employeeFile,Path hdfsPath) {
        this.employeeFile = employeeFile;
        this.OUTPUT = hdfsPath;
        init();
    }

    private void init(){

        try {
            conf = new Configuration();
            fileSystem = FileSystem.get(conf);
            writer = getSequenceWriter(conf,OUTPUT);
            employees = new ArrayList<>();
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    //Populates employees arraylist (proto objects) with data from CSV Files
    public ArrayList<Employee.Builder> populateProto() {

        boolean columnRead = false;
        Reader reader = null;

        try {
            reader = Files.newBufferedReader(Paths.get(employeeFile));
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);
            for (CSVRecord record : records) {
                if (columnRead) {
                    setProtoFields(record);
                }
                columnRead = true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return employees;

    }

    //Function to write the data from protoObjects to HDFS SEQUENCE FILE
    @Override
    public void writeToSequenceFile() {

        int rowNum = 0;
        {
            try {

                if(!fileSystem.exists(OUTPUT)){
                    System.out.println("File already exists");
                }
                else {
                    System.out.println("File doesn't exist, creating file..");
                }

                for(Employee.Builder employee : employees){
                    writer.append(new IntWritable((rowNum++)),
                            new ImmutableBytesWritable(employee.build().toByteArray()));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            fileSystem.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setProtoFields(CSVRecord record) {

        //"name","employee_id","building_code","floor_number","salary","department"
        Employee.Builder employee = Employee.newBuilder();

        employee.setName(record.get(0))
            .setEmployeeId(Integer.parseInt(record.get(1)))
            .setBuildingCode(Integer.parseInt(record.get(2)))
            .setSalary(Integer.parseInt(record.get(4)))
            .setDepartment(record.get(5));

        Employee.FLOOR_NUMBER floor = Employee.FLOOR_NUMBER.valueOf(record.get(3));
        employee.setFloor(floor);

        //adding employee proto to list
        employees.add(employee);
    }

    @Override
    public SequenceFile.Writer getSequenceWriter(Configuration config, Path path) throws IOException {
        IntWritable key = new IntWritable();
        ImmutableBytesWritable value = new ImmutableBytesWritable();
        return SequenceFile.createWriter(config, SequenceFile.Writer.file(path),
                SequenceFile.Writer.keyClass(key.getClass()),
                ArrayFile.Writer.valueClass(value.getClass()));
    }


}
