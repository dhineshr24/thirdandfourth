package com.protoassignments.first;

import com.protoobjects.Building;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import util.SequenceFileWriterInterface;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class BuildingProtoData implements SequenceFileWriterInterface {

    private String buildingFile;
    private Path OUTPUT;
    private ArrayList<Building.Builder> buildings;

    Configuration conf;
    FileSystem fileSystem;
    SequenceFile.Writer writer;


    public BuildingProtoData(String buildingFile, Path hdfsPath) {
        this.buildingFile = buildingFile;
        this.OUTPUT = hdfsPath;
        init();
    }

    private void init(){

        try{
            conf  = new Configuration();
            writer = getSequenceWriter(conf,OUTPUT);
            fileSystem = FileSystem.get(conf);
            buildings = new ArrayList<>();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //function to load data from CSV files and load into protoOBJECTS
    public ArrayList<Building.Builder> populateProto() {

        boolean columnRead = false;
        Reader reader = null;

        try {
            reader = Files.newBufferedReader(Paths.get(buildingFile));
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);
            for (CSVRecord record : records) {
                if (columnRead) {
                    setProtoFields(record);
                }
                columnRead = true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buildings;
    }

    //Function to write the data from protoObjects to HDFS SEQUENCE FILE
    @Override
    public void writeToSequenceFile() {

        int rowNum = 0;
        {
            try {
                if(!fileSystem.exists(OUTPUT)){
                    System.out.println("File already exists");
                }
                else {
                    System.out.println("File doesn't exist, creating file..");
                }

                for(Building.Builder employee : buildings){
                    writer.append(new IntWritable((rowNum++)),
                            new ImmutableBytesWritable(employee.build().toByteArray()));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            fileSystem.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setProtoFields(CSVRecord record) {

        Building.Builder building = Building.newBuilder();
        //building_code, total_floors, companies_in_the_building, cafteria_code
        building.setBuildingCode(Integer.parseInt(record.get(0)));
        building.setTotalFloors(Integer.parseInt(record.get(1)));
        building.setCompaniesInTheBuilding(Integer.parseInt(record.get(2)));
        building.setCafeteriaCode(Integer.parseInt(record.get(3)));

        buildings.add(building);

    }

    @Override
    public SequenceFile.Writer getSequenceWriter(Configuration config, Path path) throws IOException {
        IntWritable key = new IntWritable();
        ImmutableBytesWritable value = new ImmutableBytesWritable();
        return SequenceFile.createWriter(config, SequenceFile.Writer.file(path),
                SequenceFile.Writer.keyClass(key.getClass()),
                ArrayFile.Writer.valueClass(value.getClass()));
    }


}
